package ee.itcollege.i377.isesesievtoo4;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class BorderController {

	private Logger log = LoggerFactory.getLogger(BorderController.class);
	
	@RequestMapping(value="/BorderGuard/View", method=RequestMethod.GET)
	public String viewGuard(){
		
		return "borderGuards";
	}
		
	@RequestMapping(value="/BorderGuard/Update", method=RequestMethod.POST)
	public String borderGuardFormPost(@ModelAttribute @Valid BorderGuard borderGuard, BindingResult result, Model model) {
		if (result.hasErrors()) {
			log.debug("Valvuri valideerimisel tekkis viga");
			return "borderGuard";
		}
		log.debug("Adding a new guard. Name: " + 
				borderGuard.getName() + " Age: " + 
				borderGuard.getAge() + " ID: " + 
				borderGuard.getIdentificationCode());
		return "borderGuard";
	}
	
	@RequestMapping(value="/BorderPoint/View", method=RequestMethod.GET)
	public String viewBorderPoint(){
		
		return "borderPoints";
	}
		
	@RequestMapping(value="/BorderPoint/Update", method=RequestMethod.POST)
	public String borderPointFormPost(@ModelAttribute @Valid BorderPoint borderPoint, BindingResult result, Model model) {
		if (result.hasErrors()) {
			log.debug("Valvuri valideerimisel tekkis viga");
			return "borderPoint";
		}
		log.debug("Success");
		return "borderPoint";
	}
	
}
