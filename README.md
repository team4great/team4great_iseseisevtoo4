Iseseisev t�� nr 4
-------------------
*Kolm olemit:
	**Guard
	**Piiripunkt
	**Intsident

_____________________
*Olemite kirjeldused
_____________________
Piiripunktis t��tab mitu piirivalvurit. Piiripunktil toimub intsidente. Intsidendit on seotud �he piiripunktiga,
kuid �he intsidendiga v�ib olla seotud mitu piirivalvurit.

_____________________
*Vaated:
_____________________
* Anon��mselt saab ligi vaatamise kuvadele. /BorderGuard/View, /BorderPoint/View
* Muutmise kuvadele saavad ligi k�ik autenditud kasutajad. /BorderGuard/Update, /BorderPoint/Update
* Kustutamise kuvadele saab ligi vaid administrator rolliga kasutaja. N�iteks /BorderGuard/Delete, /BorderPoint/Delete

_____________________
*JSR-303
_____________________
